package net.ddns.endercrypt;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class MoveSyncListener
{
	public static void bind(Component source, Component target)
	{
		InternalListener internalListener = new InternalListener(source, target);
		source.addComponentListener(internalListener);
	}

	private static class InternalListener implements ComponentListener
	{
		private Component source;
		private Component target;
		private Point location;

		public InternalListener(Component source, Component target)
		{
			this.source = source;
			this.target = target;
			updateLocation();
		}

		private void updateLocation()
		{
			location = source.getLocation();
		}

		@Override
		public void componentResized(ComponentEvent e)
		{
			// TODO Auto-generated method stub
		}

		@Override
		public void componentMoved(ComponentEvent e)
		{
			// get location
			Point newLocation = e.getComponent().getLocation();
			Point offsetLocation = new Point(location);
			offsetLocation.x = newLocation.x - offsetLocation.x;
			offsetLocation.y = newLocation.y - offsetLocation.y;

			// move
			Point current = target.getLocation();
			current.translate(offsetLocation.x, offsetLocation.y);
			target.setLocation(current);

			// update
			updateLocation();
		}

		@Override
		public void componentShown(ComponentEvent e)
		{
			// TODO Auto-generated method stub
		}

		@Override
		public void componentHidden(ComponentEvent e)
		{
			// TODO Auto-generated method stub
		}
	}
}
