package net.ddns.endercrypt.javascript;

import java.util.Optional;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class CompiledJavascript
{
	private ScriptEngine engine;
	private Bindings bindings;
	private String code;

	private CompiledScript script;

	private String errorString = null;

	protected CompiledJavascript(ScriptEngine engine, String code) throws ScriptException
	{
		this.engine = engine;
		this.bindings = engine.getBindings(ScriptContext.GLOBAL_SCOPE);
		this.code = code;

		executeJavascriptBlock(() -> {
			script = ((Compilable) engine).compile(code);
			return null;
		});
	}

	public ScriptEngine getEngine()
	{
		return engine;
	}

	public Bindings bindings()
	{
		return bindings;
	}

	public String getCode()
	{
		return code;
	}

	public Optional<String> getErrorString()
	{
		return Optional.ofNullable(errorString);
	}

	private Object executeJavascriptBlock(JavascriptBlock javascriptBlock) throws ScriptException
	{
		try
		{
			try
			{
				return javascriptBlock.call();
			}
			catch (NullPointerException | ClassCastException e)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(e.getClass().getSimpleName());

				if (e.getMessage() != null)
				{
					sb.append(": ");
					sb.append(e.getMessage());
				}

				throw new ScriptException(sb.toString());
			}
		}
		catch (ScriptException e)
		{
			errorString = e.getMessage();
			throw e;
		}
	}

	public static String exceptionPrint(Exception e)
	{
		try
		{
			try
			{
				throw e;
			}
			catch (NullPointerException | ClassCastException e2)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(e2.getClass().getSimpleName());

				if (e2.getMessage() != null)
				{
					sb.append(": ");
					sb.append(e2.getMessage());
				}

				return sb.toString();
			}
		}
		catch (ScriptException e2)
		{
			return e2.getMessage();
		}
		catch (Exception e2)
		{
			// ignore
		}
		return null;
	}

	private interface JavascriptBlock
	{
		public Object call() throws ScriptException;
	}

	public Object eval() throws ScriptException
	{
		return executeJavascriptBlock(() -> {
			return script.eval(bindings);
		});
	}
}
