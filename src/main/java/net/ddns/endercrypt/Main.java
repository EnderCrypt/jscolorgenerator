package net.ddns.endercrypt;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.TextArea;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.Function;

import javax.script.ScriptException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Main
{
	private static BufferedImage surface;

	private static JFrame drawWindow;

	private static TextArea javascriptTextArea;
	private static JFrame codeWindow;

	private static ImageGenerator imageGenerator;

	public static void main(String[] args) throws ScriptException
	{
		// javascript
		int width = 0;
		int height = 0;
		try
		{
			Function<String, Integer> filter = (input) -> {
				if (input == null) // prompt cancelled
				{
					System.exit(0);
				}
				int number = Integer.parseInt(input);
				if (number < 0)
				{
					throw new NumberFormatException();
				}
				return number;
			};

			width = filter.apply(JOptionPane.showInputDialog(null, "Surface width:", 1000));

			height = filter.apply(JOptionPane.showInputDialog(null, "Surface height:", 750));
		}
		catch (NumberFormatException e)
		{
			JOptionPane.showMessageDialog(null, "width/height must be a number and above 0", "bad size",
					JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		// image
		surface = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		// draw panel
		JPanel drawPanel = new JPanel()
		{
			private static final long serialVersionUID = -7009779647790123690L;

			/**
			 * 
			 */

			@Override
			protected void paintComponent(Graphics g)
			{
				// surface
				g.drawImage(surface, 0, 0, null);

				// check if image generator is active
				if (imageGenerator == null)
				{
					return;
				}

				// check if error
				Optional<String> errorOptional = imageGenerator.getErrorString();
				if (errorOptional.isPresent())
				{
					// error message
					String errorString = errorOptional.get();
					FontMetrics fontMetrics = g.getFontMetrics();
					g.setColor(Color.WHITE);
					g.fillRect(0, 0, fontMetrics.stringWidth(errorString) + 16, fontMetrics.getHeight() + 10);
					g.setColor(Color.RED);
					g.drawString(errorString, 10, 16);
				}
				else
				{
					// scanline
					int scanLine = imageGenerator.getY();
					g.setColor(Color.WHITE);
					g.drawLine(0, scanLine, surface.getWidth(), scanLine);
				}
			}
		};
		drawPanel.setPreferredSize(new Dimension(surface.getWidth(), surface.getHeight()));

		// image window
		drawWindow = new JFrame("Image");
		drawWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		drawWindow.add(drawPanel);
		drawWindow.pack();
		drawWindow.setLocationRelativeTo(null);
		drawWindow.setResizable(false);
		drawWindow.setVisible(true);

		// text input area for code window
		javascriptTextArea = new TextArea();
		javascriptTextArea.setPreferredSize(new Dimension(1000, 250));

		// code window
		codeWindow = new JFrame("Javascript");
		codeWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		codeWindow.add(javascriptTextArea);
		codeWindow.pack();
		codeWindow.setLocationRelativeTo(null);
		codeWindow.setVisible(true);

		// redirect focus from draw window to code window
		drawWindow.addWindowFocusListener(new WindowFocusListener()
		{
			@Override
			public void windowLostFocus(WindowEvent e)
			{
				// ignore
			}

			@Override
			public void windowGainedFocus(WindowEvent e)
			{
				codeWindow.requestFocus();
			}
		});

		// generator thread
		imageGenerator = new ImageGenerator(surface);
		Thread generatorThread = new Thread(imageGenerator);
		generatorThread.start();

		// update thread
		Thread updateThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					while (true)
					{
						// sleep
						Thread.sleep(50);

						// text
						imageGenerator.setJavascript(javascriptTextArea.getText());

						// repaint
						drawWindow.repaint();
					}
				}
				catch (InterruptedException e)
				{
					return; // gracefully
				}
			}
		});

		updateThread.start();

		// set sync movement
		MoveSyncListener.bind(drawWindow, codeWindow);
	}
}
