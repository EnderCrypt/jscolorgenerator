package net.ddns.endercrypt;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.script.Bindings;
import javax.script.ScriptException;

import net.ddns.endercrypt.javascript.CompiledJavascript;
import net.ddns.endercrypt.javascript.JavascriptEngine;

public class ImageGenerator implements Runnable
{
	private static JavascriptEngine javascriptEngine = new JavascriptEngine();

	private BufferedImage surface;

	private boolean block = true;
	private Object blockLock = new Object();

	private int x = 0;
	private int y = 0;
	private CompiledJavascript javascript = null;

	private int r;
	private int g;
	private int b;

	private String errorString = null;

	public ImageGenerator(BufferedImage surface)
	{
		this.surface = surface;
		block = true;
	}

	private void initCompiledJavascript(CompiledJavascript compiledJavascript)
	{
		Bindings bindings = compiledJavascript.bindings();
		bindings.put("width", surface.getWidth());
		bindings.put("height", surface.getHeight());

		bindings.put("setRed", (Consumer<Number>) (red) -> {
			r = colorRange(red.doubleValue());
		});
		bindings.put("setGreen", (Consumer<Number>) (green) -> {
			g = colorRange(green.doubleValue());
		});
		bindings.put("setBlue", (Consumer<Number>) (blue) -> {
			b = colorRange(blue.doubleValue());
		});
	}

	private static int colorRange(double value)
	{
		if (value > 255)
		{
			value = 255;
		}
		if (value < 0)
		{
			value = 0;
		}
		return (int) Math.round(value);
	}

	public void setJavascript(String javascriptCode)
	{
		if (javascriptCode.trim().equals(""))
		{
			block = true;
			return;
		}
		if ((javascript != null) && (javascript.getCode().equals(javascriptCode)))
		{
			return;
		}

		// vars
		x = 0;
		y = 0;
		errorString = null;
		try
		{
			javascript = javascriptEngine.compile(javascriptCode);
		}
		catch (ScriptException e)
		{
			errorString = CompiledJavascript.exceptionPrint(e);
			block = true;
			javascript = null;
			return;
		}

		initCompiledJavascript(javascript);

		// reset block flag
		if (block)
		{
			block = false;
			synchronized (blockLock)
			{
				blockLock.notify();
			}
		}
	}

	public JavascriptEngine getJavascriptEngine()
	{
		return javascriptEngine;
	}

	public Optional<String> getErrorString()
	{
		return Optional.ofNullable(errorString);
	}

	public CompiledJavascript getJavascript()
	{
		return javascript;
	}

	public boolean isBlocked()
	{
		return block;
	}

	public int getY()
	{
		return y;
	}

	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				while (block)
				{
					synchronized (blockLock)
					{
						blockLock.wait(); // javascript has an error or is finished, lets wait for script to change
					}
				}

				// obtain a local instance
				CompiledJavascript localJavasctript = javascript;
				if (localJavasctript == null)
				{
					continue;
				}

				// execute javascript
				r = 0;
				g = 0;
				b = 0;

				// set values
				localJavasctript.bindings().put("x", x);
				localJavasctript.bindings().put("y", y);

				// execute javascript
				try
				{
					localJavasctript.eval();
				}
				catch (ScriptException e)
				{
					block = true;
					errorString = localJavasctript.getErrorString().get();
					continue;
				}

				// set surface pixel
				surface.setRGB(x, y, new Color(r, g, b).getRGB());

				// increment position
				x++;
				if (x >= surface.getWidth())
				{
					x = 0;
					y++;
					if (y >= surface.getHeight())
					{
						block = true;
						saveImage();

						continue;
					}
				}
			}
		}
		catch (InterruptedException e)
		{
			return; // gracefully
		}
	}

	private static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH_mm_ss");

	public void saveImage()
	{
		String date = dateFormat.format(Calendar.getInstance().getTime());

		try
		{
			Path imagesDirectory = Paths.get("images");
			Files.createDirectories(imagesDirectory);
			Path imageFile = imagesDirectory.resolve(date + ".png");
			ImageIO.write(surface, "PNG", imageFile.toFile());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}